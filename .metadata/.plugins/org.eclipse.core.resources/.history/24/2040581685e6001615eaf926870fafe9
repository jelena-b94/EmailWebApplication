/**
 * 
 */
package resources.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.email.Email;
import beans.user.User;
import dao.DataDao;

/**
 * Resource class that takes care of AJAX calls 
 * from JavaScript file
 * 
 * @author Jelena Banjac
 *
 */
@Path("/user")
public class UserResources {
	
	/**
	 * Start AJAX call that is executed on
	 * web application startup.
	 * @param context - context of the application (JSON)
	 * @return user - user from the session
	 */
	@POST @Path("/checkLogin")
	@Produces({ MediaType.APPLICATION_JSON})
	public User checkLogin(@Context HttpServletRequest context) {
		User user = null;
		if (context.getSession().getAttribute("user") != null) {
			 user = (User) context.getSession().getAttribute("user");
		}
		return user;
	}
	
	/**
	 * AJAX call that is executed when 
	 * client wants to register new user on the system.
	 * @param user - user that wants to be registered (JSON)
	 * @return user - user that is successfully registered, otherwise null (JSON)
	 */
	@POST @Path("/register")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON})
	public User register(User user) {
		DataDao dao = new DataDao();
		System.out.println("Registring user "+user.getUsername());
		String command = "$REGISTER " + user.getUsername();
		if (dao.register(command)==true) {
			return user;
		} else {
			return null;
		}
	}

	/**
	 * AJAX call that is executed when 
	 * client wants to login into the system.
	 * @param context - context of the application
	 * @param user - user that wants to be logged in (JSON)
	 * @return user - user that is successfully logged in, otherwise null (JSON)
	 */
	@POST @Path("/login")
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public User login(@Context HttpServletRequest context, User user) {		
		DataDao dao = new DataDao();
		System.out.println("Logging user "+user.getUsername());
		String command = "$LOGIN " + user.getUsername();
		if (dao.login(command)==true) {
			context.getSession().setAttribute("user", user);
			return user;
		} else {
			return null;
		}
	}
	
	/**
	 * AJAX call that is executed when 
	 * client wants to list all the received e-mails.
	 * @param user - user that sent request
	 * @return emails - list of received e-mails for the current user
	 */
	@POST @Path("/receivedAll")
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Email> receivedAll(User user) {
		System.out.println("\nListing received mails for user "+user.getUsername());
		DataDao dao = new DataDao();
		String command = "~"+user.getUsername()+"~LIST";
		dao.mainLoop(command);
		List<Email> emails = dao.getListServerResponse();
		return emails;
	}
	
	/**
	 * 
	 * @param emailId
	 * @param username
	 * @return
	 */
	@GET @Path("{username}&{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Email findById(@PathParam("id") String emailId, @PathParam("username") String username) {
		System.out.println("\nFind email by id "+emailId);
		DataDao dao = new DataDao();
		String command = "~"+username+"~RECEIVE "+emailId;
		dao.mainLoop(command);
		System.out.println(command);
		Email email = dao.getListServerResponse().get(0);
		System.out.println(email.getEmailId()+" "+email.getSubject());
		return email;

	}
	
	@POST @Path("/send")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String sendEmail(Email email) {
		System.out.println("\nSend email "+email.getToUser());
		DataDao dao = new DataDao();
		//String command = "~" + user.getUsername() + "~LIST";
		String command = "~"+email.getFromUser()+"~SEND TO: "+email.getToUser()+" SUBJECT: "+email.getSubject()+" TEXT: "+email.getContent();
		dao.mainLoop(command);
		System.out.println(command);
		String response = dao.getStringServerResponse();
		System.out.println(email.getEmailId()+" "+email.getSubject());
		//dao.closeSocket();
		//dao.closeSocket();
		return response;

	}
	
	@POST @Path("/logoff")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void logoff(@Context HttpServletRequest context, User user) {
		System.out.println("\nLogoff "+user.getUsername());
		DataDao dao = new DataDao();
		//String command = "~" + user.getUsername() + "~LIST";
		String command = "~"+user.getUsername()+"~LOGOFF";
		dao.mainLoop(command);
		System.out.println(command);
		context.getSession().setAttribute("user", null);
		//System.out.println(email.getEmailId()+" "+email.getSubject());
		//dao.closeSocket();
		//dao.closeSocket();

	}
}
