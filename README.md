Email Web Application
=====================

## Task 1:
   Console client-server application for email service.  
   Java documentation is generated from the code.  

## Task 2:
   Page in Web Browser receives data from Apache Tomcat using AJAX.  
   Media type in communication is JSON.  
   Data is stored on Email Server (from task 1). Socket is opened on 
   browser client in order to gather data from Email Server.  
   Protocol implemented in the Task 1 is used to exchange data between Browser client and Email Server.  
   Java documentation is generated from the code.  
   
   ![Architecture](architecture.png?raw=true "System architecture for the Task2")

----------------------------------------

### Environment:
- Eclipse IDE

----------------------------------------

### Server:
- Tomcat v7.0

----------------------------------------

### Set variable:
- Change absolute paths to JSON files for your computer.
- JSON files location:
   1. for the application
      ```
      WebContent/users.json
      WebContent/emails.json
       ```
   2. for the testing
      ```
      WebContent/users-test.json
      WebContent/emails-test.json
      ```
- Classes that need to be modified:
   1. for the application
      ```
      dao.email.Email
      dao.user.User
      ```
   2. for the testing
      ```
      test.dao.*
      ```

----------------------------------------

### Run the application:

#### Task 1 
   1. Start Application Server (Run As -> Java Application)
   ```
    console.server.EmailServer
   ```
   2. *N x* Start Aplication Client (Run As -> Java Application)
   ```
    console.client.EmailClient
   ```
   3. Imitate console client 

#### Task 2
   1. Start Task 1 Application Server (Run As -> Java Application)
   ```
    EmailWebApplication project -> console.server.EmailServer 
   ```
   2. Start Task 2 Application Server - Tomcat (Run As -> Run on Server)
   ```
    EmailWebApplicationBrowser project
   ```
   3. Imitate browser client in browser (e.g. try one in default window and another one in icognito)
   ```
    http://localhost:8080/EmailWebApplicationBrowser/ 
   ``` 

----------------------------------------

### Test the application:
   1. Start Test User DAO (Run As -> JUnit test)
   ```
    test.dao.TestUserDao
   ```
   2. Start Test Email DAO (Run As -> JUnit test)
   ```
    test.dao.TestEmailDao
   ```
   
----------------------------------------

### Author:
   Jelena Banjac  
   SW 16/2013  
